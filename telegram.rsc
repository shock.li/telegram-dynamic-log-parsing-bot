# Author: P.J. Coetzee; shock.jpc2000@gmail.com; IRC on freenode as shocj, tripsit/lsd25 as shock; 
# Purposefully left open for the person who will contribute to this amazing project one day.

# This project is licensed under the GNU General Public License v3.0. Learn more here choosealicense.com/licenses/gpl-3.0/

# BEGIN SETUP
:local scheduleName "telegram"
# Messages to search for in the log
:local startBuf [:toarray [/log find message~"logged in" || message~"login failure" || message~"user" || message~"telnet" || message~"Port Scan" || message~"l7"]]
# Get a Bot ID: https://core.telegram.org/bots
:local botURL "botNNNNNNNN:xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
# Get your Chat ID via the web bot api. Psssst: /getMe & /getUpdates
:local chatID "-323305157"
:local apiURL "https://api.telegram.org"
# Exclude some content
:local removeThese {"123.123.123.123"}
:local mtikname [/system identity get name]
:local currentIP [:resolve myip.opendns.com server=208.67.222.222];
# END SETUP

:if ([:len [/system scheduler find name="$scheduleName"]] = 0) do={
	/log warning "[TG-API] ERROR: Schedule does not exist. Create schedule and edit script to match name"
}

:local lastTime [/system scheduler get [find name="$scheduleName"] comment]
:local currentTime
:local message 
:local output
:local keepOutput false

:if ([:len $lastTime] = 0) do={
	:set keepOutput true
}

:local counter 0
:foreach i in=$startBuf do={
	:local keepLog true
	:foreach j in=$removeThese do={
		:if ([/log get $i message] ~ "$j") do={
			:set keepLog false
		}
	}

	:if ($keepLog = true) do={
		:set message [/log get $i message]
		:set currentTime [/log get $i time]
		:if ([:len $currentTime] = 8 ) do={
			:set currentTime ([:pick [/system clock get date] 0 11]." ".$currentTime)
		} else={
			:if ([:len $currentTime] = 15 ) do={
				:set currentTime ([:pick $currentTime 0 6]."/".[:pick [/system clock get date] 7 11]." ".[:pick $currentTime 7 15])
			}
		}

		:if ($keepOutput = true) do={
			:set output ($message."\r\n")
		}

		:if ($currentTime = $lastTime) do={
			:set keepOutput true
			:set output ""
		}
	}

	:if ($counter = ([:len $startBuf]-1)) do={
		:if ($keepOutput = false) do={
			:if ([:len $message] > 0) do={
				:set output ($message."\r\n")
			}
		}
	}

	:set counter ($counter + 1)
}

if ([:len $output] > 0) do={
	/system scheduler set [find name="$scheduleName"] comment=$currentTime
	/tool fetch url="$apiURL/$botURL/sendMessage?chat_id=$chatID&parse_mode=markdown&text=*Device Name:* $mtikname%0A*Public IP:* $currentIP%0A*Time:* $currentTime%0A*Info:*%0A$output" keep-result=no
	/log info "[TG-API] INFO: New logs found, sent to telegram."
}

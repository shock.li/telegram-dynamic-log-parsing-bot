# Telegram Dynamic Log Parsing Bot

This bot started out as first simply to send a message to a group whenever there's a failed authentication. 
During its changes and fixes over time it was no longer a "simple" bot, but one that can alert you immediately when there is a problem.

You will notice, which is a bit weird, that there is a license to this script. If you are to be using this script, no matter what device the license and script must be available for anybody to read. 

For example a Mikrotik device has an HTTP server and an FTP server; by not running these services and preventing the license and software becoming available you will be violating the license.
